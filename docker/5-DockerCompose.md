# 1 - **DOCKER COMPOSE**

## Installation

Docker compose n'est pas forcément installé par defaut sur la machine, sur notre debian de test, il faudra l'installer.

    vagrant@master:~$ sudo curl -L https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose && sudo chmod +x /usr/local/bin/docker-compose

Ce curl permet de disposer d'une version récente de docker-compose, mais si vous pouvez vous contenter d'une version plus ancienne, on peut installer celle de debian.

    vagrant@master:~$ sudo apt install --no-install-recommends docker-compose

A noter que le programme est écrit en python, d'ou l'installation de dépendance liées a python

## Un fichier docker-compose.yml

Il se configure avec docker-compose.yml.

Se fichier est la définition de l'application.

- Services (micro services)
- Volumes
- Networks
- Secrets (Swarm)
- Configs (Swarm)

La documentation complete se trouve sur [docs.docker.com](https://docs.docker.com/compose/compose-file/compose-file-v3/)

Un exemple simple du fichier en question :

    version: '3.8'
    volumes:
        data:
    networks:
        frontend:
        backend:
    services:
        web:
            image: org/web:2.3
            networks:
                - frontend
            ports:
                - 80:80
        db:
            image: postgres:alpine
            volumes:
                - data:/data/db
            networks:
                - backend

Dans notre exemple, on commence pas indiquer la version de docker-compose, elle permet de ne pas avoir de problème avec la compatibilité du descripteur.

On décrit globalement quel volumes et quel networks seront utilisés par l'ensemble des services.

Puis chaques services, on retrouve ce qui l'on passe habituellement a docker run

A noter que le nom des services, exemple web et db seront utilisable dans le conteneur pour votre code source.

Exemple en php, on utilise db a la place de l'ip ou localhost pour se connecter a mariadb

    mysql_connect("db", "mysql_user", "mysql_password")

Vous trouverez un autre exemple dans cette voting app dispo sur [github](https://github.com/dockersamples/example-voting-app)

## Les options de docker-compose

|Commande|Utilisation|
|--------|----------:|
| up     | Création et démarrage de l'application|
| down   | suppression de l'application|
| build  | Si utilisation de l'instuction build|
| pull   | Télécharge les images de l'application|
| logs   | Visualisation des logs de l'application|
| scale  | modification du nombre de conteneur d'un service|
| ps     | List des containers de l'application|

## Exemple develop

On regarde l'application en mode developpement [docker-compose-simple.yml](https://github.com/dockersamples/example-voting-app/blob/master/docker-compose-simple.yml)

    version: "3"

    services:
    vote:
        build: ./vote
        command: python app.py
        volumes:
        - ./vote:/app
        ports:
        - "5000:80"

On voit qu'il est composé de build, qui indique l'endroit ou se trouve le contexte de build du service (avec le Dockerfile)

Ce repertoire contient donc tous les élements necessaire a la construction de ce service et de l'image docker.

command redéfini le CMD du conteneur.

Le volume map directement le fichier sources avec l'app, c'est utilisé en environement de développement, en production l'image devra contenir directement l'application pour des raisons de sécurité.

## Example production

En prenant la meme application mais en version production [docker-stack.yml](https://github.com/dockersamples/example-voting-app/blob/master/docker-stack.yml)

    db:
        image: postgres:9.4
        environment:
        POSTGRES_USER: "postgres"
        POSTGRES_PASSWORD: "postgres"
        volumes:
          - db-data:/var/lib/postgresql/data
        networks:
          - backend
        deploy:
            placement:
                constraints: [node.role == manager]
    vote:
        image: dockersamples/examplevotingapp_vote:before
        ports:
          - 5000:80
        networks:
          - frontend
        depends_on:
          - redis
        deploy:
            replicas: 2
            update_config:
                parallelism: 2
            restart_policy:
                condition: on-failure

Ce coup ci on n'a plus la mention de build, mais on a directement l'utilisation d'une image buildée contenant directement le source.

L'instruction deploy ajouté depuis la version 3 du format précise certaines action du conteneur comme le service est démarré uniquement sur le node manager pour la db, que 2 conteneurs seront lancé pour vote, ou encore qu'en cas de defaillance, le conteneur sera redémarré automatiquement.

## docker-compose up

    vagrant@master:~$ sudo apt install git
    vagrant@master:~$ git clone https://github.com/dockersamples/example-voting-app.git
    vagrant@master:~$ cd example-voting-app/

    vagrant@master:~/example-voting-app$ docker-compose -f docker-compose-simple.yml up -d

on execute le fichier compose et on demande de rendre la main une fois terminé avec -d

Le build est assez long.

## docker-compose ps

    vagrant@master:~/example-voting-app$ docker-compose ps
            Name                          Command               State                      Ports
    -------------------------------------------------------------------------------------------------------------------
    example-voting-app_db_1       docker-entrypoint.sh postgres    Up      5432/tcp
    example-voting-app_redis_1    docker-entrypoint.sh redis ...   Up      0.0.0.0:32768->6379/tcp
    example-voting-app_result_1   docker-entrypoint.sh nodem ...   Up      0.0.0.0:5858->5858/tcp, 0.0.0.0:5001->80/tcp
    example-voting-app_vote_1     python app.py                    Up      0.0.0.0:5000->80/tcp
    example-voting-app_worker_1   dotnet Worker.dll                Up

il suffit d'aller avec le navigateur http://IP_HOTE:5000/ pour voter
et sur http://IP_HOTE:5001/ pour voir le résultat du vote.

## Scaling

actuellement un seul conteneur de worker tourne, on peut le monter facilement a 2 avec

    vagrant@master:~/example-voting-app$ docker-compose up -d --scale worker=2

Et après une 30aine de secondes on se retrouve avec un nouveau worker.

    vagrant@master:~/example-voting-app$ docker-compose ps
            Name                          Command                  State                          Ports
    --------------------------------------------------------------------------------------------------------------------------
    example-voting-app_db_1       docker-entrypoint.sh postgres    Up (healthy)   5432/tcp
    example-voting-app_redis_1    docker-entrypoint.sh redis ...   Up (healthy)   0.0.0.0:32771->6379/tcp
    example-voting-app_result_1   docker-entrypoint.sh nodem ...   Up             0.0.0.0:5858->5858/tcp, 0.0.0.0:5001->80/tcp
    example-voting-app_vote_1     python app.py                    Up             0.0.0.0:5000->80/tcp
    example-voting-app_worker_1   dotnet Worker.dll                Up
    example-voting-app_worker_2   dotnet Worker.dll                Up

Si on essai de scaler vote qui expose un port, il va y avoir une erreur.

## docker-compose stop/start

    vagrant@master:~/example-voting-app$ docker-compose stop

cette commande stop les servives, on peut les redamarrer avec start

    vagrant@master:~/example-voting-app$ docker-compose start

## docker-compose down

La commande down effectue un stop, mais supprime aussi toutes les ressources, sauf les volumes, si on veut supprimer aussi les volumes il faut lui passer l'option -v

    vagrant@master:~/example-voting-app$ docker-compose down -v