# 1 - **DOCKER SWARM**

Swarm est un orechestrateur.

On définit une application avec docker-compose et on la déploie dans le cluster avec swarm.

Un load balancing de bas niveau est intégré, et au niveau du conteneur, les ip sont géré via des VIP.

## Les primitives

Il y a des primitives qui ont étaient choisit, il faut donc les connaitres.

| primitive | description |
|-----------|------------:|
| node      | Machine embre d'un cluster Swarm|
| service   | Spécification des containers d'une application|
| stack     | groupe de services|
| secret    | données sécurisé (mot de passe.. )|
| config    | configuration de l'application|

---
# Les nodes


## Creation d'un docker swarm

**Pour les exemples, j'ai créé 3 machines sous debian avec vagrant node0, node1 et node2**

    vagrant@node0:~$ docker swarm init --advertise-addr 192.168.56.50
    Swarm initialized: current node (wv98elrj0y9c87g3axlzjmvsm) is now a manager.

    To add a worker to this swarm, run the following command:

        docker swarm join --token SWMTKN-1-2oqe0n28tice6vg38d0fob4ddjck0vcezherw1sbkibccihekd-e8p7kwndvggnlr1b98dnytiwz 192.168.56.50:2377

    To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.

La commande et très simple, le parametre advertise-addr est utilisé car j'ai plusieurs ip sur ma machine hote.

## Creation d'un worker

Et bien sur, sur notre node 1, on tape la commande qu'il donne.

    vagrant@node1:~$ docker swarm join --token SWMTKN-1-2oqe0n28tice6vg38d0fob4ddjck0vcezherw1sbkibccihekd-e8p7kwndvggnlr1b98dnytiwz 192.168.56.50:2377
    This node joined a swarm as a worker.

## Raft, l'algorithme de consensus distribué

Il suffit de regarder cette animation : http://thesecretlivesofdata.com/raft/

## Nodes Manager et Worker

Certains nodes sont dédiés a la gestion du clustern, ce sont les nodes manager.
Les nodes worker sont dédiés a l'application.

Les Managers :
- Schedule et orchestre les containers
- gère l'état du cluster
- consensus RAFT

Les Workers :
- Ecécute les containers

Par défaut, un manager est aussi un worker.

## Les etats du node

- Active
    * Le scheduler peut assignet des tache a ce node
- Pause
    * Le scheduler ne peut pas assigner de tache
    * Les taches tournant sur ce node sont inchangées
- Drain
    * Le scheduler ne peut pas assigner de tache
    * Les taches sont stoppés et sont relancées sur un autre node

## docker node

docker node présente différentes commandes et elles doivent etre addéssées a des managers.

Commands:
  * demote      Demote one or more nodes from manager in the swarm
  * inspect     Display detailed information on one or more nodes
  * ls          List nodes in the swarm
  * promote     Promote one or more nodes to manager in the swarm
  * ps          List tasks running on one or more nodes, defaults to current node
  * rm          Remove one or more nodes from the swarm
  * update      Update a node

## docker node ls

    vagrant@node0:~$ docker node ls
    ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
    m6yosa9cr94l3zv4r9nzuxov4 *   node0               Ready               Active              Leader              18.09.1
    n6gflfu2i7u2sqhy75lt7o96b     node1               Ready               Active                                  18.09.1

Cette commande ne fonctionne que sur le node manager, indiqué "Leader" sur le ls.

## Ajout d'un 2eme Manager

Losque on a fait l'init, il a donné une commande pour creer un worker, mais aussi pour creer un manager.

Sur notre manager actuel node0

    vagrant@node0:~$ docker swarm join-token manager
    To add a manager to this swarm, run the following command:

        docker swarm join --token SWMTKN-1-2oqe0n28tice6vg38d0fob4ddjck0vcezherw1sbkibccihekd-cf1e3zee3xsp6dcw8ko9xzj2m 192.168.56.50:2377

Et elle affiche un autre token qui n'est pas le meme que le premier, celui ci permet de creer un nouveau manager, sur node 2 on execute la commande.

    vagrant@node2:~$ docker swarm join --token SWMTKN-1-2oqe0n28tice6vg38d0fob4ddjck0vcezherw1sbkibccihekd-cf1e3zee3xsp6dcw8ko9xzj2m 192.168.56.50:2377
    This node joined a swarm as a manager.

Et comme node 2 est devenu manager, node ls fonctionne.

    vagrant@node2:~$ docker node ls
    ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
    762jvb21cz9vqlzqj1qctc9ms     node0               Ready               Active              Leader              18.09.1
    w7pqh8ui02zy5pw2x521tjorh     node1               Ready               Active                                  18.09.1
    i5e1z9r17pnj4x27v997un2fk *   node2               Ready               Active              Reachable           18.09.1

Comme il est en backup du leader, il est indiqué reachable.

## Un manager devient worker.

Il est possible de transformer un manager en worker avec la commande node demote.

sur notre manager node0, on va le passer en worker.

    vagrant@node0:~$ docker node demote node0
    Manager node0 demoted in the swarm.

on ne peut plus faire de node ls sur notre node0, mais si on le fait sur celui qui était en backup, il a bien été promu

    vagrant@node2:~$ docker node ls
    ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
    762jvb21cz9vqlzqj1qctc9ms     node0               Ready               Active                                  18.09.1
    w7pqh8ui02zy5pw2x521tjorh     node1               Ready               Active                                  18.09.1
    i5e1z9r17pnj4x27v997un2fk *   node2               Ready               Active              Leader              18.09.1

## Un worker devient manager

    vagrant@node2:~$ docker node promote node1
    Node node1 promoted to a manager in the swarm.

## docker node update --availability

Il existe des instructions pour la maintenance de node, exemple mise a jour en modifiant l'instruction availability

il existe 3 paramètres :
- pause : met en pause le node, il ne recevra plus de tache
- drain : transfère les conteneurs qu'il traite vers un autre node (un pause bien amélioré)
- active : disponible pour traitement des conteneurs.

Si on passe en drain le node1 depuis node 2 :

    vagrant@node2:~$ docker node update --availability drain node1
    node1

## docker node update --label

Depuis le node manager on peut set un label sur n'importe quel node.

    vagrant@node0:~$ docker node update --label-add ssd=true node2

## node inspect

    vagrant@node0:~$ docker node inspect -f '{{json .Spec.Labels }}' node2
    {"ssd":"true"}

Par exemple, si on detect ssd true, on sait que ce node est disponible avec un disque dur performant et il pourra lancer des conteneurs spécifiques.

---
# Les services

Un service décrit comment un conteneur doit etre lancé.

Il est lancé en mode réplicat (plusieurs conteneur sur un node) ou global (un conteneur par node, généralement pour le monitoringU)

Le service possède une VIP, les services utilsent le nom du service pour communiquer

Un DNS round robin est opéré sur les services pour balancer la charge entre les services répliqués.

Et pour finir, un routing mesh est utilisé pour publier un port, si on ouvre le port 443 par exemple, il sera ouvert automatiquement sur tous les nodes.

Un service execute une à n tache (une tache par conteneur)

docker service expose 

|command   |description|
|----------|----------:|
create     | Create a new service
inspect    | Display detailed information on one or more services
logs       | Fetch the logs of a service or task
ls         | List services
ps         | List the tasks of one or more services
rm         | Remove one or more services
rollback   | Revert changes to a service's configuration
scale      | Scale one or multiple replicated services
update     | Update a service

## docker service create

    vagrant@node0:~$ docker service create --name www -p 80:80 --replicas 3 nginx:alpine
    3uifpvb8dsb75j9bwo73gqe1q
    overall progress: 3 out of 3 tasks
    1/3: running   [==================================================>]
    2/3: running   [==================================================>]
    3/3: running   [==================================================>]
    verify: Service converged

La commande ressemble étrangement a docker run, mais en plus on a le nombre de replicas de ce service.

## docker service ls

Permet juste de lister les services et leur nombre de replicat.

    vagrant@node0:~$ docker service ls
    ID                  NAME                MODE                REPLICAS            IMAGE               PORTS
    3uifpvb8dsb7        www                 replicated          3/3                 nginx:alpine        *:8080->80/tcp

## docker service ps

Affiche ou les services sont en train de fonctionner.

    vagrant@node0:~$ docker service ps www
    ID                  NAME                IMAGE               NODE                DESIRED STATE       CURRENT STATE           ERROR               PORTS
    8gflmpbspex4        www.1               nginx:alpine        node1               Running             Running 4 minutes ago
    rc36rs2x4eq7        www.2               nginx:alpine        node0               Running             Running 4 minutes ago
    ufohwknuw48x        www.3               nginx:alpine        node2               Running             Running 4 minutes ago

## upscale ou downscale

On peut changer le nombre de replicats a la volée.

    vagrant@node0:~$ docker service scale www=1
    www scaled to 1

Et effectivement, il n'y en a plus que 1 qui tourne.

    vagrant@node0:~$ docker service ps www
    ID                  NAME                IMAGE               NODE                DESIRED STATE       CURRENT STATE           ERROR               PORTS
    rc36rs2x4eq7        www.2               nginx:alpine        node0               Running             Running 6 minutes ago

Si vous allez sur les 3 ip des nodes avec le navigateur, vous verez que les 3 ip ont bien nginx sur leur port 80, pourtant un seul conteneur tourne sur le node 0 dans notre cas. C'est le mécanismes routing mesh qui s'occupe de ça.

A noter que l'equivalent de la commande scale est 

    vagrant@node0:~$ docker service update --replicas 1 www

## Arret des services

    vagrant@node0:~$ docker service rm www

rm détruit les services.


## service de visualisation

    vagrant@node0:~$ docker service create \
       --name visualizer \
       --mount type=bind,source=/var/run/docker.sock,destination=/var/run/docker.sock \
       --constraint 'node.role == manager' \
       --publish "8000:8080" dockersamples/visualizer:stable
    
On va lancer le visualizer de chez docker, a noter le parametre qui impose au service de fonctionner seulement sur la manager.

en se connectant a l'ip d'un note :8000 sur le navigateur on peut voir ou sont les services.

![visualizer](visualizer.png)

Si on pase le node 1 en drain, on autre node prendra son service.


## Rolling ugprade

On va recreer notre service nginx

    vagrant@node0:~$ docker service rm www
    vagrant@node0:~$ docker service create --update-parallelism 1 --update-delay 10s -p 80:80 --replicas 3 --name www nginx:1.20-alpine

lors de la mise a jour de l'image, on précie qu'il devrait le faire un service après l'autre espacé de 10 secondes.

On veut mettre a jour l'image et passer en nginx 1.21

    vagrant@node0:~$ docker service update --image nginx:1.21-alpine www

On précise a la fin quel service mettre a jour.

La mise a jour est graduelle et il n'y a pas eu d'interuption de service.

## rollback

Si on a fait une erreur on peut revenir en arrière très facilement sur la version précédente visible dans l'inspecteur.

    vagrant@node0:~$ docker service inspect -f '{{json .PreviousSpec }}' www|jshon

et pour ce roolback :

    vagrant@node0:~$ docker service rollback www

et il consever la meme gestion de parallelism et update delay.

    vagrant@node0:~$ docker service ps www
    ID                  NAME                IMAGE               NODE                DESIRED STATE       CURRENT STATE             ERROR               PORTS
    kyaqmdtqtbra        www.1               nginx:1.20-alpine   node1               Running             Running 53 seconds ago
    iztdbhkiy8lt         \_ www.1           nginx:1.21-alpine   node1               Shutdown            Shutdown 53 seconds ago
    rs1j6ujv157h         \_ www.1           nginx:1.20-alpine   node1               Shutdown            Shutdown 7 minutes ago

et on note bien l'historique avec ps.

## rollback automatique

lors du service create on peut lui passer le parametre "--update-failure-action rollback" qui indique si il y a eu un problème (HEALTHCHECK qui retourne une erreur), swarm effectura un rollback automatique.

---
# Secret

Depuis la version 1.13, secret est disponible pour stocker les données dite sensible tel que les mots de passe.

Les mots de passent sont disponible en clair uniquement dans les conteneurs qui utilisent ce secret, dans le repertoire TMPFS /run/secrets/SECRET

Les secrets sont passés au conteneur via Raft de façon chiffré.

# docker secret create

    vagrant@node0:~$ echo "changeme"|docker secret create password123 -
    pnf4uq49wube3itn98f259o0v

dans cet exemple, je donne le mot de passe "changeme" il il sera stocké de façon chiffré dans password123

Ensuite si j'ajoute un conteneur en lui précisant ce secret

    vagrant@node0:~$ docker service create -p 80:80 --replicas 3 --name www --secret=password123 nginx:alpine

Je rentre ensuite dans le conteneur du node 0, le fichier contient bien password123 en clair.

    vagrant@node0:~$ docker exec -ti $(docker ps --filter name=www -q) sh
    / # cat /run/secrets/password123
    changeme

On peut ajouter un autre password au conteneur

    vagrant@node0:~$ echo "test2"|docker secret create password456 -
    i0o3iu45lswquhqkfj2r5pqc1
    vagrant@node0:~$ docker service update --secret-add=password456 www

Et bien sur on peut supprimer un secret d'un conteneur

    vagrant@node0:~$ docker service update --secret-rm=password456 www

# docker secret ls

On peut lister les secrets

    vagrant@node0:~$ docker secret ls
    ID                          NAME                DRIVER              CREATED             UPDATED
    pnf4uq49wube3itn98f259o0v   password123                             11 minutes ago      11 minutes ago
    i0o3iu45lswquhqkfj2r5pqc1   password456                             2 minutes ago       2 minutes ago

# docker secret rm

Et les effacer si ils ne sont pas utilisés par un conteneur.

    vagrant@node0:~$ docker secret rm password456

---
# Config

Les config fonctionnent exactement de la meme façon que secret a la différence que config n'est pas chiffré.

    vagrant@node0:~$ docker config --help

# EXEMPLE Nginx en SSL

On va creer un serveur nginx qui écoute en 443, la configuration et le crt sera passé par config, et la clé privé par secret.

On commence par creer le fichier ssl.conf

    vagrant@node0:~$ cat ssl.conf
    server {
        listen              443 ssl;
        server_name         test.com;
        ssl_certificate     /etc/ssl/certs/server.crt;
        ssl_certificate_key /etc/ssl/certs/server.key;
        ssl_protocols       TLSv1.1 TLSv1.2;
        ssl_ciphers         HIGH:!aNULL:!MD5;
        location / {
            root   /usr/share/nginx/html;
            index  index.html index.htm;
        }
    }

On génère la clé privé de notre CA

    vagrant@node0:~$ openssl genrsa -out ca-key.pem 4096

On génère le certificat correspondant

    vagrant@node0:~$ openssl req -new -x509 -days 365 -key ca-key.pem -sha256 -subj "/C=FR/L=Paris/O=orig/CN=ca" -out ca.pem

Et on génère la clé privé pour nginx

    vagrant@node0:~$ openssl genrsa -out server-key.pem 4096

Le CSR

    vagrant@node0:~$ openssl req -new -subj "/C=FR/L=Paris/O=orig/CN=test.com" -key server-key.pem -out server.csr

Et on termine par le certificat

    vagrant@node0:~$ openssl x509 -req -days 365 -in server.csr -CA ca.pem -CAkey ca-key.pem -CAcreateserial -out server-cert.pem

Et on construit notre fichier docker compose.

    vagrant@node0:~$ cat nginx-ssl.yml
    version: '3.3'
    services:
        www:
            image: nginx:alpine
            configs:
              - source: server_config
                target: /etc/nginx/conf.d/ssl.conf
              - source: server_cert
                target: /etc/ssl/certs/server.crt
            secrets:
              - source: server_key
                target: /etc/ssl/certs/server.key
            ports:
              - "80:80"
              - "443:443"
            deploy:
                restart_policy:
                    condition: on-failure
    configs:
        server_config:
            file: ./ssl.conf
        server_cert:
            file: ./server-cert.pem
    secrets:
        server_key:
            file: ./server-key.pem

En bas du fichier, configs précise que la variable se nome server_config et son fichier sur le hote ssl.conf
Ensuite, dans le service, il suffit d'intique cette variable en source et la destination mettre celle qui nous interesse.

Et on peut démarre notre docker compose swarm avec 

    vagrant@node0:~$ docker stack deploy -c nginx-ssl.yml app

---
# docker stack

Stack est un groupe de services qui est déployés avec un fichier docker compose.

Exemple avec la [voting app](https://github.com/dockersamples/example-voting-app/blob/master/docker-stack.yml)

## Exemple avec swamprom

On va deployer la stack [swamprom](https://github.com/stefanprodan/swarmprom/) qui est un outil de monitoring pour docker.

    vagrant@node0:~$ git clone https://github.com/stefanprodan/swarmprom.git
    vagrant@node0:~$ cd swarmprom/
    vagrant@node0:~$ ADMIN_USER=admin ADMIN_PASSWORD=admin SLACK_URL=https://hooks.slack.com/services/TOKEN SLACK_CHANNEL=devops-alerts SLACK_USER=alertmanager docker stack deploy -c docker-compose.yml mon

Et on peut voir l'utilisation de votre docker swarm sur ces url en remplaçant par l'ip d'un de vos node swarm bien sur.
- Utilisation des services : http://192.168.56.50:3000/dashboard/db/docker-swarm-services
- Utilisation des nodes : http://192.168.56.50:3000/dashboard/db/docker-swarm-nodes

Information interessante que l'on peut voir dans docker-compose.yml

      resources:
        limits:
          memory: 128M
        reservations:
          memory: 64M

On peut limiter un conteneur en ressource, dans cet exemple on reserve 64Mo de ram pour ce conteneur et si il dépasse 128Mo il sera automatiquement redémarré.

## docker stack ls

Permet de voir qu'elle stack est en train de fonctionners

    vagrant@node0:~/swarmprom$ docker stack ls
    NAME                SERVICES            ORCHESTRATOR
    mon                 8                   Swarm

## docker stack services

En passant le nom de la stack, on a une visualisation de quel service tourne

    vagrant@node0:~/swarmprom$ docker stack services mon
    ID                  NAME                   MODE                REPLICAS            IMAGE                                          PORTS
    0ebos455zb9r        mon_grafana            replicated          1/1                 stefanprodan/swarmprom-grafana:5.3.4
    cripxsccq4n9        mon_prometheus         replicated          1/1                 stefanprodan/swarmprom-prometheus:v2.5.0
    igg48kb95zjt        mon_alertmanager       replicated          1/1                 stefanprodan/swarmprom-alertmanager:v0.14.0
    nxd926lwugxn        mon_node-exporter      global              3/3                 stefanprodan/swarmprom-node-exporter:v0.16.0
    ps0ufacikoq9        mon_unsee              replicated          1/1                 cloudflare/unsee:v0.8.0
    wudl38wx4ljr        mon_caddy              replicated          1/1                 stefanprodan/caddy:latest                      *:3000->3000/tcp, *:9090->9090/tcp, *:9093-9094->9093-9094/tcp
    ypf0s3opfbmt        mon_cadvisor           global              3/3                 google/cadvisor:latest
    ywuwo8luvfwf        mon_dockerd-exporter   global              3/3                 stefanprodan/caddy:latest

## docker stack rm

Pour supprimer une stack il suffit la commande en ajoutant bien sur le nom de la stack

    vagrant@node0:~/swarmprom$ docker stack rm mon

---
# gestion de swarm graphique

## Exemple : Portainer

[Portainer](https://documentation.portainer.io/v2.0/deploy/ceinstallswarm/) est une gui pour utiliser docker.

    vagrant@node0:~$ curl -L https://downloads.portainer.io/portainer-agent-stack.yml -o portainer-agent-stack.yml
    vagrant@node0:~$ docker stack deploy -c portainer-agent-stack.yml portainer

Puis rendez vous sur http://192.168.56.50:9000/

Si on edite le fichier yml on voit que

    deploy:
      mode: global

Le service agent est en mode global, il doit tourner sur tous les nodes.

Alors que portainer est en clasique replicated et ne doit tourner que sur le manager

    deploy:
      mode: replicated
        replicas: 1
      placement:
        constraints: [node.role == manager]

Et un network est créé

    networks:
      - agent_network

Qui permet aux 2 services de dialogués entre eux de façon "étanche"

## Swarmpit

Comme portainer, [Swarmpit](https://github.com/swarmpit/swarmpit#installation) est un gestionnaire de docker swarm.

    vagrant@node0:~$ docker stack rm portainer
    vagrant@node0:~$ git clone https://github.com/swarmpit/swarmpit -b master
    vagrant@node0:~$ docker stack deploy -c swarmpit/docker-compose.yml swarmpit

Swarmpit est disponible sur http://192.168.56.50:888/

Dans le fichier yml on note

    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:8080"]
      interval: 60s
      timeout: 10s
      retries: 3

L'utilisation d'un healtcheck tout simple basé sur la réponse curl.

    deploy:
        resources:
            limits:
                cpus: '0.50'
                memory: 1024M
            reservations:
                cpus: '0.25'
                memory: 512M

La limitation de ressource en ram et cpu

    environment:
      - DOCKER_API_VERSION=1.35

Et le passage en env d'une variable

