# 1 - **DOCKER CONTAINER**

Pour ce Mémo, on se trouve avec un environnement Debian Buster, vous pouvez donc installer dans une machine virtuelle l’iso correspondant.

On peut bien sur installer notre debian via [Vagrant](https://gitlab.com/ccham/share/-/blob/main/Vagrantfile).

---
## Préparation à l'utilisation
---

On va commencer par installer docker.

    root@master:/home/vagrant# apt update
    root@master:/home/vagrant# apt --no-install-recommends install docker.io

Tout simple, docker est installé et il est déja fonctionnel.

    root@master:/home/vagrant# docker --version
    Docker version 18.09.1, build 4c52b90

---
## docker container
---
Dans ce chappitre, on va voir la commande **docker container**, mais bonne nouvelle, étant la commande la plus utilisé, le "container" est opptionnel.

## docker run _image_
Notre premier conteneur, qui affiche juste bonjour, nous détailleront docker run juste après ps.

    docker run hello-world

On trouve les images disponibles sur le site [hub.docker.com](https://hub.docker.com/search?q=hello-world&type=image)

## docker ps

Cette commande permet de connaitre quel process sont en train de tourner

    root@master:/home/vagrant# docker ps
    CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
    139bc3ebeab1        alpine              "sleep 1000"        3 seconds ago       Up 2 seconds                            sleep

## docker ps -a

Cette commane comme la précédente permet de voir les conteneurs en trains de fonctionner, mais aussi ceux qui ont terminés.

    root@master:/home/vagrant# docker ps -a
    CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                     PORTS               NAMES
    0bd05c279fa1        hello-world         "/hello"            6 seconds ago       Exited (0) 4 seconds ago                       gallant_hawking
    139bc3ebeab1        alpine              "sleep 1000"        2 minutes ago       Up 2 minutes                                   sleep

## docker ps -aq

Cette commande retourne les **ID** de tous les conterneurs (enlever le a si on ne veut que ceux qui tournent)

    root@master:/home/vagrant# docker ps -aq
    0bd05c279fa1
    139bc3ebeab1
C'est surtout utilise pour scripter des actions.

## docker rm **_ID_ou_nom_**

Docker rm permet de supprimer une image qui ne tourne plus et que l'on peut voir avec **docker ps -a**

rm ne peut supprimer que des images qui sont stoppées, mais on peut le forcer a stopper puis supprimer avec le modificateur -f

    root@master:/home/vagrant# docker rm 0bd05c279fa1
    0bd05c279fa1

Ou tout tuer et supprimer en récuppérent les infos de docker ps

    root@master:/home/vagrant# docker rm -f $(docker ps -aq)
    139bc3ebeab1

## docker pull **image**

docker pull permet de charger une image distante en local avant de l'executer avec docker run.

    root@master:/home/vagrant# docker pull alpine:latest
    latest: Pulling from library/alpine
    Digest: sha256:eb3e4e175ba6d212ba1d6e04fc0782916c08e1c9d7b45892e9796141b1d379ae
    Status: Image is up to date for alpine:latest

Une image _alpine_ est ce qui se fait de plus petit en image, et le _:latest_ précise le tag à utiliser. Sur la page [docker hub d'alpine](https://hub.docker.com/_/alpine) on peut voir les _Supported tags_ disponibles.
Par defaut si on ne précise pas le tag, docker utilisera latest, on autait donc pu écrire :

    docker pull alpine

Cependant, il n'est pas necessaire de faire un docker pull avant un docker run, si l'image n'est pas disponible en local, docker run fera tout seul le pull.

## docker run -ti

Si on execute notre conteneur alpine simplement avec :

    docker run alpine

Et ensuite on utilise un ps, on vera que rien ne tourne, car cette image ne fait rien de particulier, elle est surtout la si on veut creer nous meme de toute petite image, d'ailleurs beaucoup d'image sont basées sur alpine.

On peut demander au run de lancer un terminal interactif directement après démarrage, et la notre conteneur aura quelque chose a faire, vu qu'il va executer **sh**

    root@master:/home/vagrant# docker run -ti alpine
    / #

Et la on se retrouve avec un petit linux dont les commandes de base fonctionnent.

Lorsque l'on va quitter ce terminal avec CTRL-D le conteneur sera immédiatement détruit vu qu'il n'a plus rien a faire tourner.

## docker run __image__ **commande**

    docker run alpine ping 1.1.1.1

on execute ping 1.1.1.1 depuis notre conteneur alpine. on peut utiliser CTRL-C pour quitter la commande (a condition que la commande en question reponde au kill)

## docker run -d image

-d permer de lancer en sous tache (daemonize) le conteneur, très utilse si on execute une action style notre ping infini ou encore un conteneur nginx par exemple.

    root@master:/home/vagrant# docker run -d debian:buster ping 1.1.1.1
    8b11619aff02696aa433bf6cb9b32c3f8c803355f81cd3d3b75809181640bfb0

La commande rend immédiatement la main en précisant l'id long du conteneur en question.

Si on fait un docker ps on peut effectivement voir que le ping est en cours d'execution dans le conteneur debian buster.

A noter la dernière colone de ps qui indique le nom attribué au conteneur.

On peut stopper ce conteneur avec **docker stop**.

    root@master:/home/vagrant# docker stop zen_morse
    zen_morse

Il peut arriver qu'il ne stoppe pas, il suffit de faire CTRL-C puis de relancer la commande.
Si le binaire lancé ne répond pas au kill d'arret propre, comme sleep par exemple, on peut utiliser **docker kill ID** pour qu'il utilise un kill -9 sur le process.

## docker run --name "NOM"

--name permet de spécifier un nom a un conteneur au lieu qu'un aléatoire (comme zen_morse) lui soit attribué et cela permet d'agir plus facilement sur le conteneur qu'aved un ID.

    root@master:/home/vagrant# docker run -d --name monping debian:buster ping 1.1.1.1

et ensuite on intervient sur le conteneur avec son nom

    root@master:/home/vagrant# docker kill monping
    monping


## docker stop et docker start

Comme vu avant la commande stop arrète un conteneur, mais elle peut aussi le démarrer.

Laçon un conteneur qui fournit un service nginx.

    root@master:/home/vagrant# docker run -d --name web nginx:stable-alpine

avec docker ps, on peut donc voir notre conteneur nginx qui porte le nom de web etre en train de tourner, on peut donc l'arreter avec

    root@master:/home/vagrant# docker stop web

le ps confirme qu'il ne tourne plus, mais on peut le relancer

    root@master:/home/vagrant# docker start web

## docker exec

docker exec permet d'executer un programme dans un docker qui est en train de fonctionner, reprenons le cas de notre nginx. (le relancer si vous l'avez éteint)

    root@master:/home/vagrant# docker exec -ti web sh
    / #

on execute sh sur notre conteneur nommé web ce qui nous donne donc un prompt. (bash n'est pas installé sur une image alpine)

**Remarque IMPORTANTE :**

si on fait un ps sur ce shell, on va voir que le process 1 est utilsé par nginx
````
/ # ps
PID   USER     TIME  COMMAND
    1 root      0:00 nginx: master process nginx -g daemon off;
24 nginx     0:00 nginx: worker process
25 nginx     0:00 nginx: worker process
31 root      0:00 sh
38 root      0:00 ps
````

Oui un docker execute toujours en 1 le process principal, attetion, lors d'un docker stop c'est toujours le process 1 qui reçoit le kill, il faut donc que ce process soit capable de lui meme de stopper ces childs.

## docker run -p _rediction de port_

Notre nginx tourne sur le port 80, mais on ne peut pas acceder a ce port 80 car la machine hote n'a pas de liaison avec ce port.

On va relancer notre conteneur nginx en y précisant de rediriger son port 80 interne vers le 8080 externe (de la machine hote)

    root@master:/home/vagrant# docker rm -f $(docker ps -aq)
    root@master:/home/vagrant# docker run -d --name web -p 8080:80 nginx:stable

on redirige le port 8080 de la machine hote vers le port interne 80 du conteneur.

    root@master:/home/vagrant# docker ps
    CONTAINER ID        IMAGE                 COMMAND                  CREATED             STATUS              PORTS                  NAMES
    db0f5bbc3cdf        nginx:stable-alpine   "/docker-entrypoint.…"   5 seconds ago       Up 3 seconds        0.0.0.0:8080->80/tcp   web

La colonne PORTS nous indique que la redirection est valide, vous pouvez tester sur votre navigateur en utilisant http://ip_de_votre_machine_hote:8080

## docker inspect ID

docker inspect permet de connaitre plein d'information sur notre conteneur et les data sont facilement parsable car au format json.

    root@master:/home/vagrant# docker inspect web

L'option -f permet de passer une query.

    root@master:/home/vagrant# docker inspect -f {{.NetworkSettings.IPAddress}} web
    172.17.0.2

L'exemple nous retourne l'ip interne de ce conteneur.

Ou on peut voir ce qui nous interesse la redirection de port.

    root@master:/home/vagrant# docker inspect -f {{.NetworkSettings.Ports}} web
    map[80/tcp:[{0.0.0.0 8080}]]

## Modification du conteneur.

On va modifier une donnée dans le conteneur nginx précédent (bash fonctionne car stable n'est pas basé sur alpine)

--rm permet de supprimer ce conteneur quand il sera terminé, ça évite d'ensuite faire un docker rm web
    
    root@master:/home/vagrant# docker rm -f web
    root@master:/home/vagrant# docker run -d --rm --name web -p 8080:80 nginx:stable
    root@master:/home/vagrant# docker exec -ti web bash
    root@cf03f1d65ba4:/# apt -qq update && apt -qq install -y vim

On edit le fichier html que retourne nginx a vote navigateur.

    root@cf03f1d65ba4:/# vim /usr/share/nginx/html/index.html

faite une modif puis F5 sur le navigateur, cette modification est bien prise en compte.

On peut stopper et redémarrer le conteneur, pas de problème, cette modification sera toujours la.

Mais, si on supprime le conteneur et on le relance, cette modification ne sera plus la (ni vim d'ailleurs)

    root@master:/home/vagrant# docker run -d --rm --name web -p 8080:80 nginx:stable

et on peut faire un F5 sur la page du navigateur.

## docker run -v

Comme on a vu précédemment, on perd toute modification si on relance un conteneur, c'est la qu'interviennent les volumes.

    root@master:/home/vagrant# mkdir /var/www
    root@master:/home/vagrant# echo "Hello world" > /var/www/index.html
    root@master:/home/vagrant# docker run -d --rm -p 8080:80 -v /var/www/:/usr/share/nginx/html/ --name web nginx:stable

Grace à l'option -v, maintenent, le repertoire /usr/share/nginx/html se trouvant dans le conteneur est mappé sur notre hote porteur /var/www.  
Meme si on relance le conteneur, les fichiers modifiés n'étant plus dans le conteneur, le contenu n'est donc plus dépendant de la vie du conteneur.

Pour plus de sécurité on peut ajouter :ro après le mappage si on veut que le montage soit en lecture seule, dans ce cas le conteneur ne pourra pas écrire dans ce répertoire et donc ne pourra pas modifier l'hote.

    root@master:/home/vagrant# docker run -d --rm -p 8080:80 -v /var/www/:/usr/share/nginx/html/:ro --name web nginx:stable

## docker run avec limitation de ressources

Si on ne veut pas allouer toutes la puissance de la machine hote a notre docker, on peut ajouter quelques parametres.

avec l'option --memory-swap -1 le swap du conteneur est désactivé.

avec l'option --memory on peut limiter la conso de ram du conteneur, attention, c'est une limite depuis la machine hote au niveau cgroup, dans le conteneur on verra toujours la ram de l'hote, mais on ne pourra consommer que 32Mo dans l'exemple.

    root@master:/home/vagrant# docker run --rm --memory 32M --memory-swap -1 --name ramtest estesp/hogit

Cette image est un process qui consomme de la ram en boucle.

On va lancer une image qui consomme du cpu

    root@master:/home/vagrant# docker run -d --rm --name consocpu progrium/stress --cpu 2

la commande en fin qui est passé en argument au cunteneur indique combien de core on veut consommer.  
Sans limitation, le conteneur a accès a toutes les ressources de l'hote, soit 2 cores.

    %Cpu0  :100.0 us,  0.0 sy,  0.0 ni,  0.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
    %Cpu1  : 98.7 us,  1.3 sy,  0.0 ni,  0.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st

l'option --cpus de docker run permet de limiter la conso cpu, l'unité est quota de cpu, en gros 1 indique 100% de cpu, si on veut utiliser 2 cores on mettra 2 et si on veut utiliser seulement 50% d'un core 0.5

    root@master:/home/vagrant# docker run -d --cpus 0.5 --rm --name consocpu progrium/stress --cpu 2

## docker avec auto restart

Une option très utile permet de redémarrer automatiquement le conteneur en cas de plantage, non compatible avec l'option --rm bien sur.

    root@master:/home/vagrant# docker run -d --restart=on-failure --name plante alpine false

Ce conteneur qui execute false fait croire que l'application c'est mal arrétée, donc il est relancé en boucle. 

    root@master:/home/vagrant# docker ps
    CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                         PORTS               NAMES
    b7765d14a8ff        alpine              "false"             11 seconds ago      Restarting (1) 3 seconds ago                       plante

## docker logs

Dans un docker, il est déconseillé de mettre les logs dans un fichier, mais il faut plutot écrire dans stdout. (sauf a mapper le repertoire logs avec un volumme bien sur)

pour consulter les logs d'un conteneur, la commande logs permet de le faire, l'option -f (comme pour tail) permet de voir les logs en temps reel.

    root@master:/home/vagrant# docker run -d --name monping alpine ping 1.1.1.1
    root@master:/home/vagrant# docker logs -f monping
    PING 1.1.1.1 (1.1.1.1): 56 data bytes
    64 bytes from 1.1.1.1: seq=0 ttl=62 time=1.569 ms

## Passer des variables par l'environnement

On peut forcer des variables directement dans l'environnement du conteneur.

    root@master:/home/vagrant# docker run -ti --rm --name mondebian --env MAVARIABLE="1234" debian:buster
    root@7bac512b3069:/# echo $MAVARIABLE
    1234

lors de l'execution de ce conteneur, une varibla d'environnement sera disponible avec 1234 a l'intérieur.

Si on veut passer plusieurs variable il faut mieux passer par un fichier texte, chaque variable différentes seront a la ligne.

    root@master:/home/vagrant# cat my.env
    login="identifiant"
    pass="motdepasse"

Et l'option --env-file permet de passer ce fichier au conteneur.

    root@master:/home/vagrant# docker run -ti --rm --name mondebian --env-file ./my.env debian:buster
    root@835d82cbe624:/# env|egrep "(login|pass)"
    pass="motdepasse"
    login="identifiant"


## Des bash alias

voici quelques alias qui peuvent servir pour les commandes les plus courrantes, a placer dans son .bashrc bien sur.

    alias dps='docker ps'
    alias dpsa='docker ps -a'
    alias dstop='docker stop $(docker ps -aq)'
    alias drm='docker rm (docker ps -aq)'
    alias drmf='docker rm -f (docker ps -aq)'
    dshell () {
            (docker exec -ti $1 bash) || (docker exec -ti $1 sh)
    }
