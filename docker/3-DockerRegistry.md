# 1 - **DOCKER REGISTRY**

Un registry comme docker hub est un endroit ou télécharger (pull) et uploader (push) des images docker.

sur [hub.docker.com](https://hub.docker.com/search?q=&type=image&image_filter=official) il est conseillé de privilégier les images officieles qui sont sure.

A noter que les images accès sécurité Alpine sont un très bon choix en plus d'etre le plus légère possible.


## Les différents registry

Les registry officiels

- docker hub : celui qui est le plus connu et il est activé par defaut sur docker
- docker registry : Une solution opensource pour creer son propre registry, disponible avec une image docker hub.
- docker trusted registry : une version commercial, disponible avec Docker-EE

Les registry alternatifs

- Amazon EC2 container registry
- Google Container Registry
- Quay.io (CoreOS)
- GitLab container registry
- ...

## hub.docker.com

Identifiez vous sur hub.docker.com et aller hub.docker.com/repositories

On va creer un nouveau repository, par exemple ping qui sera le nom de notre image.

A noter, dans la version gratuite de docker hub, on n'a le droit qu'a un seul repository privé.

On va creer notre image ping avec le Dockerfile suivant 

    FROM alpine:latest
    ENTRYPOINT ["ping"]
    CMD ["1.1.1.1"]

Et on build notre image ping

    vagrant@master:~$ docker build -t ping:v1.0 .

pour l'uploader il faut qu'elle ait un nome constitué de : identifiant_docker_hub/repository_docker_hub:tag

## docker image tag

Comme on peut le faire avec git, on peut creer un tag sur une image existante, on va en profiter pour utiliser le format attendu par docker hub.

    vagrant@master:~$ docker image tag ping:v1.0 ccham/ping:v1.0
    vagrant@master:~$ docker images
    REPOSITORY          TAG                 IMAGE ID            CREATED              SIZE
    ccham/ping          v1.0                497ed6c9d860        About a minute ago   5.6MB
    ping                v1.0                497ed6c9d860        About a minute ago   5.6MB
    alpine              latest              021b3423115f        4 days ago           5.6MB


## docker login

Permet de s'authentifier sur le registry (hub.docker.com par défaut)

    vagrant@master:~$ docker login
    Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
    Username: ccham
    Password:

C'est sur docker login ou on peut choisir un autre registry

    vagrant@master:~$ docker login registry.gitlab.com

## docker image pull

    docker image pull alpine:latest

Ce cas que l'on connait est très simple, on télécharge l'image alpine:latest depuis le registry par defaut, soit hub.docker.com

## docker image push

    vagrant@master:~$ docker image push ccham/ping:v1.0
    The push refers to repository [docker.io/ccham/ping]
    bc276c40b172: Pushed
    v1.0: digest: sha256:788a88b90de078ea7ad0f33940e7bc9f0cebe9009c3d45ed0f5fcb51a6cc8787 size: 528

---
## Creer son propre registry
---

On va creer un registry basé sur l'image officiele docker hub, sachant que le conteneur registry expose le port 5000.

    vagrant@master:~$ docker run -d -p 5000:5000 --name maregistry registry

Pour lister les images de cette registry, il faut faire une simple requete curl.

    vagrant@master:~$ curl localhost:5000/v2/_catalog
    {"repositories":[]}

on fait un tag correspondant a notre registry soit localhost:5000/repository:tag

    docker image tag ping:v1.0 localhost:5000/ping:v1.0

et on peut envoyer notre image sur ce registry

    vagrant@master:~$ docker image push localhost:5000/ping:v1.0
    The push refers to repository [localhost:5000/ping]
    bc276c40b172: Pushed
    v1.0: digest: sha256:bbc073d3cc179ddd546d7c62f73599a522a9af47894ce40d641742c09917c837 size: 528
    vagrant@master:~$ curl localhost:5000/v2/_catalog
    {"repositories":["ping"]}

Seule localhost est authorisé dans ce cas, si on essai de faire un tag avec l'ip de l'hote, le push sera refusé.

Pour authoriser de communiquer sans certificats, il faut indiquer au docker démon que notre ip:port est insecure.

    vagrant@master:~$ cat /etc/docker/daemon.json
    {
            "insecure-registries": ["192.168.56.50:5000"]
    }

et bien sur redémarer le service.

    sudo systemctl restart docker

## Notre registry en TLS

On ajoute un repertoire certs et on se déplace dedans.

    vagrant@master:~$ sudo su
    root@master:/home/vagrant# mkdir /etc/docker/certs
    root@master:/home/vagrant# cd /etc/docker/certs

Creer un fichier cnf qui comprend bien sur notre dns et l'ip de notre registry.

    root@master:/etc/docker/certs# cat registry.cnf
    [ req ]&
    prompt                 = no
    default_bits           = 4096
    default_md             = sha256
    distinguished_name     = req_distinguished_name
    x509_extensions        = v3_req

    [ req_distinguished_name ]
    CN = registry.mydom.com

    [ v3_req ]
    keyUsage = nonRepudiation, digitalSignature, keyEncipherment
    subjectAltName = @alt_names

    [ alt_names ]
    DNS.1 = maregistry.localdomain
    IP.1  = 192.168.56.50

Les machines voulant pousser dans cette registry devront avoir dans le /etc/hosts

    192.168.56.50 maregistry.localdomain

Puis avec openssl on génère certificat et clé privé autosigné.

    root@master:/etc/docker/certs# openssl req -x509 -newkey rsa:4096 -nodes -sha256 -days 3650 \
    -keyout domain.key \
    -out domain.crt \
    -config registry.cnf

On modifie /etc/docker/daemon.json pour ajouter maregistry.localdomain

Et maintenant on relance le docker sur le port 443 

docker run -d --restart=always --name registry \
-v /etc/docker/certs:/certs \
-e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
-e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
-e REGISTRY_HTTP_ADDR=0.0.0.0:443 \
-p 443:443 registry

Maintenant on a un registry qui repond sur le port 443 donc le port des dispensable dans le push.

    vagrant@master:~$ docker image tag ping:v1.0 maregistry.localdomain/ping:v1.0
    vagrant@master:~$ docker push maregistry.localdomain/ping:v1.0
    The push refers to repository [maregistry.localdomain/ping]
    bc276c40b172: Pushed
    v1.0: digest: sha256:bbc073d3cc179ddd546d7c62f73599a522a9af47894ce40d641742c09917c837 size: 528

