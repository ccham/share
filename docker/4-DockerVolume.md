# 1 - **DOCKER VOLUME**

Quand on démarre le conteneur, on peut écrire des données dans le dernier layer qui est en lecture écriture.

    vagrant@master:~$ docker run -ti --name testtoto alpine
    / # touch toto
    vagrant@master:~$ sudo find /var/lib/docker/overlay2 -name toto
    /var/lib/docker/overlay2/0085330947b7b273a2eb6662223b682391a9399bfe633e349865228a684fbf7e/diff/toto

Dans cet exemple, on peut voir que le fichier que j'ai ajouté dans mon conteneur alpine "toto" est bien présent sur le file système de l'hote dans un des layers.

Maintenant, je supprime ce conteneur et je refais le find, le fichier n'est plus la.

    vagrant@master:~$ docker rm testtoto
    testtoto
    vagrant@master:~$ sudo find /var/lib/docker/overlay2 -name toto

## Mount avec inspect

L'outil jdhon n'est pas forcément installé, on peut l'installer sur notre debiant avec

    vagrant@master:~$ sudo apt install jshon

Si vous voulez voir quel est le point de montage, il faut rechercher directement dans un conteneur.

    vagrant@master:~$ docker run --name some-postgres -e POSTGRES_PASSWORD=mysecretpassword -d postgres:alpine
    vagrant@master:~$ docker inspect -f '{{json .Mounts }}' some-postgres|jshon
    [
        {
            "Type": "volume",
            "Name": "41056906c9e926e3cb7df9683d1f2c482df8a3ee5ba64e80b47bc3bc3c73aac3",
            "Source": "\/var\/lib\/docker\/volumes\/41056906c9e926e3cb7df9683d1f2c482df8a3ee5ba64e80b47bc3bc3c73aac3\/_data",
            "Destination": "\/var\/lib\/postgresql\/data",
            "Driver": "local",
            "Mode": "",
            "RW": true,
            "Propagation": ""
        }
    ]

Si on se rend dans la source indiqué par ce json, effectivement on voit que des datas sont bien stockés dans le volume.

    vagrant@master:~$ sudo ls -la /var/lib/docker/volumes/3eed93ddf63bece85475331744e023ef2554e569e181cb01ce177ed3c5cb84b1/_data/
    -rw-------  1   70   70     3 Aug 11 14:45 PG_VERSION
    drwx------  5   70   70  4096 Aug 11 14:45 base
    drwx------  2   70   70  4096 Aug 11 14:46 global
    ...

Si on supprime ce conteneur, le volume lui n'est pas effacé.

    vagrant@master:~$ docker rm -f some-postgres
    vagrant@master:~$ sudo ls -la /var/lib/docker/volumes/

## docker volume inspect

La commande inspect permet d'avoir un résume du contenu d'un object, dans le cas du volume

    vagrant@master:~$ docker volume inspect 3eed93ddf63bece85475331744e023ef2554e569e181cb01ce177ed3c5cb84b1
    [
        {
            "CreatedAt": "2021-08-11T14:45:58Z",
            "Driver": "local",
            "Labels": null,
            "Mountpoint": "/var/lib/docker/volumes/3eed93ddf63bece85475331744e023ef2554e569e181cb01ce177ed3c5cb84b1/_data",
            "Name": "3eed93ddf63bece85475331744e023ef2554e569e181cb01ce177ed3c5cb84b1",
            "Options": null,
            "Scope": "local"
        }
    ]

## docker volume ls

Un ls permet de lister les conteneur, et l'option -q retourne seulement les ids.

    vagrant@master:~$ docker volume ls
    DRIVER              VOLUME NAME
    local               3eed93ddf63bece85475331744e023ef2554e569e181cb01ce177ed3c5cb84b1
    local               5f32bcd2890c8962ab5a88d3f55b44a78e562caf02ee517cfa62cae7c13a93d1

## docker volume rm

Un rm supprime un volume

    vagrant@master:~$ docker volume rm 5f32bcd2890c8962ab5a88d3f55b44a78e562caf02ee517cfa62cae7c13a93d1

## docker volume prune

Permet de supprimes les volumes qui ne sont plus utilisés par un conteneur

    vagrant@master:~$ docker volume prune

## docker volume create

Permet de creer un nouveau volume qui pourra etre utilisé par un conteneur.

    vagrant@master:~$ docker volume create www-data

Des optons sont set par défaut comme par exemple le driver local

    vagrant@master:~$ docker inspect www-data
    [
        {
            "CreatedAt": "2021-08-11T14:57:35Z",
            "Driver": "local",
            "Labels": {},
            "Mountpoint": "/var/lib/docker/volumes/www-data/_data",
            "Name": "www-data",
            "Options": {},
            "Scope": "local"
        }
    ]

## monter un volume dans un conteneur


    vagrant@master:~$ docker run -tid --name testmount --mount type=volume,source=www-data,target=/my-configs alpine
    vagrant@master:~$ docker inspect -f '{{json .Mounts }}' testmount|jshon
    [
        {
            "Type": "volume",
            "Name": "www-data",
            "Source": "\/var\/lib\/docker\/volumes\/www-data\/_data",
            "Destination": "\/my-configs",
            "Driver": "local",
            "Mode": "z",
            "RW": true,
            "Propagation": ""
        }
    ]

    la commande mount avec type=volume,source=VOLUME_NAME,target=DESTINATION_PATH

## monter le volume d'un autre conteneur

    Un conteneur peut utilise le volume d'un autre conteneur avec l'option --volumes-from

    vagrant@master:~$ docker run -tid --name testmount2 --volumes-from testmount alpine
    vagrant@master:~$ docker inspect -f '{{json .Mounts }}' testmount2|jshon
    [
        {
            "Type": "volume",
            "Name": "www-data",
            "Source": "\/var\/lib\/docker\/volumes\/www-data\/_data",
            "Destination": "\/my-configs",
            "Driver": "local",
            "Mode": "",
            "RW": true,
            "Propagation": ""
        }
    ]

    Il a repris le montage du précédent conteneur

## Exemple avec nginx

dans le cas d'nginx, on peut utiliser un mappage directe, mais aussi creer un volume vers le repertoire html d'nginx.

    vagrant@master:~$ docker volume create --name html
    vagrant@master:~$ docker run --name www -d -p 8080:80 -v html:/usr/share/nginx/html nginx:alpine


## les volumes dans un Dockerfile

Si on construit une image, on peut automatiquement creer un volume avec le mot clé VOLUME.

    FROM alpine:latest
    VOLUME ["/data"]

Puis on construit cette image, et on démarre un conteneur dessus.

    vagrant@master:~$ docker build -t data:v1.0 .
    vagrant@master:~$ docker container run --name testdata -ti data:v1.0
    / # touch /data/toto.txt
    vagrant@master:~$ docker container inspect -f "{{ json .Mounts }}" testdata
    [{"Type":"volume","Name":"35da91f3bd2cbf96954478b0f4dad6a2a5fbe3fe47cd73cebeec680054515fbf","Source":"/var/lib/docker/volumes/35da91f3bd2cbf96954478b0f4dad6a2a5fbe3fe47cd73cebeec680054515fbf/_data","Destination":"/data","Driver":"local","Mode":"","RW":true,"Propagation":""}]
    vagrant@master:~$ sudo ls -la /var/lib/docker/volumes/35da91f3bd2cbf96954478b0f4dad6a2a5fbe3fe47cd73cebeec680054515fbf/_data
    -rw-r--r-- 1 root root    0 Aug 11 15:37 toto.txt

on a bien notre fichier toto.txt de présent, et si on détruit le conteneur

    vagrant@master:~$ docker rm -f testdata

le fichier est toujours la.

## Un volume dont la source est un dossier fixe.

On peut faire un mappage direct du repertoire_hote:repertoire_conteneur

    vagrant@master:~$ docker run -d -p 8080:80 -v $(pwd):/usr/share/nginx/html/ --name web nginx:alpine

Dans ce cas, j'ai mappé /home/vagrant dans /usr/share/nginx/html/ sans avoir créé de volume

---
## Plugin de volume
---

Par defaut on a un plugin de type local.

Depuis le [Docker hub plugins](https://hub.docker.com/search?q=&type=plugin&category=volume) on peut voir les plugins disponibles pour volume.

Mais on ne voit que les plugins certifiés.

### Exemple avec SSHFS

on va installer le plugin [sshfs](https://hub.docker.com/p/vieux/sshfs)

    vagrant@master:~$ docker plugin install --grant-all-permissions vieux/sshfs
    vagrant@master:~$ docker plugin ls
    ID                  NAME                 DESCRIPTION               ENABLED
    dcded35d8cfa        vieux/sshfs:latest   sshFS plugin for Docker   true

Et on ajoute un volume de ce type (sur une vm debian lancé a vagrant juste pour le test)

    vagrant@master:~$ docker volume create -d vieux/sshfs --name ssh -o sshcmd=vagrant@192.168.56.51:/home/vagrant/ -o password=vagrant
    vagrant@master:~$ docker volume inspect ssh
    [
        {
            "CreatedAt": "0001-01-01T00:00:00Z",
            "Driver": "vieux/sshfs:latest",
            "Labels": {},
            "Mountpoint": "/mnt/volumes/668b67db7edf9e2d2c06dd6e615fa616",
            "Name": "ssh",
            "Options": {
                "password": "vagrant",
                "sshcmd": "vagrant@192.168.56.51:/home/vagrant/"
            },
            "Scope": "local"
        }
    ]

