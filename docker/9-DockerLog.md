# **DOCKER LOG**

## Bonne pratique

- Ne pas sauvegarder les logs dans le conteneur, sinon ils seraient écrit dans la layer du conteneur, et on perdrait les logs si suppression du conteneur, ou encore on n'y aurait accès difficilement.
- Log seront écrit directement dans stdout / stderr
- Utiliser un agrégateur de log

## Driver de logs

Docker utilise des drivers de logs pour écire ces logs

    vagrant@node0:~$ docker info 2>&1|grep "Plugin" -A 3|grep Log
    Log: awslogs fluentd gcplogs gelf journald json-file local logentries splunk syslog

Le driver par défaut et json-file, les logs sont écrits dans :

    vagrant@node0:~$ sudo find /var/lib/docker/containers/ -name "*.log"
    /var/lib/docker/containers/93ee8ee83147aab9d9b5ffe6e4faa9165865af2f994811c9e067ff01f270016b/93ee8ee83147aab9d9b5ffe6e4faa9165865af2f994811c9e067ff01f270016b-json.log

Le fichier /etc/docker/daemon.json permet de modifier le driver log par defaut.

Exemple avec [syslog](https://docs.docker.com/config/containers/logging/syslog/)

    {
        "log-driver": "syslog",
        "log-opts": {
            "syslog-address": "udp://1.2.3.4:1111"
        }
    }  

Ou on peut modifier le driver au lancement de la machine

     docker run --log-driver syslog --log-opt syslog-address=udp://1.2.3.4:1111 alpine echo hello world

Ou dans le docker compose yml avec

    logging:
        driver: "syslog"
        options:
            syslog-address: "udp://1.2.3.4:1111"

