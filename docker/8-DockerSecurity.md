# **Sécuriser docker**

Il est préférable d'effectuer plusieurs pratiques pour éviter une vulnérabilitée.

Pour commencer, vous pouvez passer le [docker bench security](https://github.com/docker/docker-bench-security) sur votre cluster docker.

C'est outil est conçu pour remonter les problèmes qui sont liés a votre configuration docker.

## capability

Il existe des capability qui permettent d'authoriser des actions ou de les refuser qui sont listées dans la [documentation de docker](https://docs.docker.com/engine/reference/run/#runtime-privilege-and-linux-capabilities)

Quand on lance un conteneur, il faut mentionner --cap-add ou --cap-drop si on veut ajouter ou supprimer une capacitée.

exemple pour refuser qu'une machine puisse pinguer.

    vagrant@node0:~$ docker run --name pingoff --cap-drop=NET_RAW alpine ping 1.1.1.1
    PING 1.1.1.1 (1.1.1.1): 56 data bytes
    ping: permission denied (are you root?)

## Linux security module

Il existe différentes protection comme appArmor ou selinux, les images alpine sont sous appArmor par exemple.

Le profil par defaut [apparmor](https://github.com/moby/moby/blob/master/contrib/apparmor/template.go)


L'autre systeme, SELinux et comment il fonctionne https://opensource.com/business/13/11/selinux-policy-guide

## Seccomp

Il existe plus de 300 appels systeme linux comme halt, reboot, mkdir... 

Docker en authorise 44 seulement.

il faudra passer un profil custom si on veut déverouiller des commandes particulières.

on peut s'aider de strace pour savoir quel appel est bloqué.

On peut modifier le profil json par defaut [default.json](https://github.com/moby/moby/blob/master/profiles/seccomp/default.json)

    docker run --rm -it --security-opt seccomp=./profile.json hello-world

## Scan de l'image docker

On va utiliser l'utilitaire trivy pour scanner une image

On commence par installer l'outil

    vagrant@node0:~$ curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/master/contrib/install.sh | sudo sh -s -- -b /usr/local/bin

puis on peut scanner une image

    vagrant@node0:~$ trivy nginx:alpine
    2021-08-13T22:09:57.774Z        INFO    Detected OS: alpine
    2021-08-13T22:09:57.774Z        INFO    Detecting Alpine vulnerabilities...
    2021-08-13T22:09:57.775Z        INFO    Number of language-specific files: 0

Et pour tester si ça fonctionne bien, vous pouvez scanner une vieille image nginx.

vagrant@node0:~$ trivy nginx:1.14

Et d'ailleurs en testant l'ancienne version de nginx 1.20, on peut voir qu'avec la meme version, une image alpine est bien moins touchée par les CVE.

    vagrant@node0:~$ trivy nginx:1.20|grep CVE|wc -l
    166
    vagrant@node0:~$ trivy nginx:1.20-alpine|grep CVE|wc -l
    20

## Docker content Trust

Cet outil sert a signer une image de la part de l'auteur.

Activer docker content trust.

    $ export DOCKER_CONTENT_TRUST=1

Et lors du push de l'image sur docker hub, docker va demander une pathphrase pour signer cette image.

Lors du pull, si on a activé docker content trust, on ne pourra pull que des images signer.

