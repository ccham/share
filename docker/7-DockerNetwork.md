# 1 - **DOCKER NETWORK**

Le Container Network Model (CNM) c'est :
- Sandbox
- Endpoint, une interface réseau
- Network, l'ensemble des Endpoint.

Un endpoint peut etre ataché a un seul réseau.
Une sandbox peut avoir plusieur endpoint.

## docker network

|Commands|Description|
|--------|----------:|
|connect | Connect a container to a network
create | Create a network
disconnect | Disconnect a container from a network
inspect | Display detailed information on one or more networks
ls | List networks
prune | Remove all unused networks
rm | Remove one or more networks

## docker network ls

Par defaut, docker ajoute automatiquement 3 network.

    vagrant@node2:~$ docker network list
    NETWORK ID     NAME      DRIVER    SCOPE
    f5adda1699df   bridge    bridge    local
    c2c97d4381ab   host      host      local
    19d76c132ae0   none      null      local

Bridge : permet au conteneurs de communiquer entre eux sur le meme hote
Host : accès a la stack reseau de la machine hote
none : aucun accès, le conteneur n'aura qu'une interface locale

## docker network create

    $ docker network create --driver DRIVER [OPTIONS] NAME

On peut choisir un driver, celui par defaut est bridge.

## network = bridge default

    vagrant@node2:~$ ip a show docker0
    4: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
        link/ether 02:42:88:02:29:25 brd ff:ff:ff:ff:ff:ff
        inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
            valid_lft forever preferred_lft forever
        inet6 fe80::42:88ff:fe02:2925/64 scope link
            valid_lft forever preferred_lft forever

Sur notre hote, on a un bridge docker0 qui a comme ip 172.17.0.1/16

    vagrant@node2:~$ sudo brctl show
    bridge name     bridge id               STP enabled     interfaces
    docker0         8000.024288022925       no

Le bridge est identifié avec la commande brctl show du paquet bridge-utils.

Si j'ajoute un simple conteneur alpine

    vagrant@node2:~$ docker run -tid --name alpinebridge alpine

Une interface, celle du conteneur s'attache au bridge

    vagrant@node2:~$ sudo brctl show
    bridge name     bridge id               STP enabled     interfaces
    docker0         8000.024288022925       no              veth446ad97

L'ip de notre conteneur alpine est (via inspect) : 172.17.0.2/16

Attention, il n'y a pas de résolution de nom en mode bridge par defaut, un autre conteneur attaché au bridge ne pourra pas pinger testalpine, mais pourra pinguer son ip.

## network = host

On ajoute un alpine en network host.

    vagrant@node2:~$ docker run -tid --network=host --name alpinehost alpine
    vagrant@node2:~$ docker exec -ti alpinehost sh
    / # ip link
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
        link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP qlen 1000
        link/ether 08:00:27:8d:c0:4d brd ff:ff:ff:ff:ff:ff
    3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP qlen 1000
        link/ether 08:00:27:cb:56:1d brd ff:ff:ff:ff:ff:ff
    4: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN
        link/ether 02:42:88:02:29:25 brd ff:ff:ff:ff:ff:ff

Et on se retrouve avec le reseau exact de la machine hote.

## network = none

    vagrant@node2:~$ docker run -tid --network=none --name alpinenone alpine
    vagrant@node2:~$ docker exec -ti alpinenone sh
    / # ip a
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
        link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
        inet 127.0.0.1/8 scope host lo
        valid_lft forever preferred_lft forever

Sur un network none, seule la loopback est montée

## bridge custom

on va creer un nouveau bridge avec la commande

    vagrant@node2:~$ docker network create --driver=bridge mybridge

Et on génère 2 conteneur alpine qui utilsie ce bridge

    vagrant@node2:~$ docker run -tid --network=mybridge --name alpine1 alpine
    vagrant@node2:~$ docker run -ti --network=mybridge --name alpine2 alpine

Le 1er alpine en mode daemon en sous tache, et le 2eme avec le terminal

    / # ping alpine1
    PING alpine1 (172.18.0.2): 56 data bytes
    64 bytes from 172.18.0.2: seq=0 ttl=64 time=0.209 ms

Avec le bridge custom, la résolution par nom de conteneur fonctionne

L'ip de notre machine est 172.18.0.3/16, les 2 machines alpine 1 et alpine 2 peuvent se voir mais sont isolées du reste du reseau

A noter que les conteneurs peuvent communiquer par leur nom car un serveur DNS est intégré au docker démon.

---
# Network dans SWARM

## network overlay

- Communication entre des containers sur des hotes différents
- Utilise VXLan du kernel
- Ne peut etre créé que dans un cluster.
- paire de virtual eth (veth)

Création d'un network de type overlay 

    vagrant@node2:~$ docker network create --driver=overlay netswarm

et bien sur il faut creer un service vu que l'on est sur du swarm.

    vagrant@node0:~$ docker service create --network=netswarm --replicas 3 --name alpine1 alpine

## Routing mesh

Le routing mesh inclus plusieus chose comme un load balancer ip si on utilise plusieurs replicas, mais il ouvre aussi les ports d'un service sur l'ensemble des nodes, si le node en question de comporte pas le service, les paquets seront retransmis sur un node portant le service via iptable.

![mesh](mesh.png)

