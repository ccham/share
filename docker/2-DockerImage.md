# 1 - **DOCKER IMAGE**

Une image est composée de 1 à N couche (layer) qui sont en lecture seule.  

Le composant **graph driver** s'occupe d'unifier l'ensemble de ces couches en un seul file system toujours en lecture seule.  

Quand le conteneur est extensié, le **graph driver** ajoute une nouvelle couche en lecture écriture et elle permet de modifier les file system sans que les layers de l'image ne soit impacté.

Si on modifie un fichier d'un layer en lecture seule, la fonction **copy-on-write** copie automatiquement le fichier dans le layer en écriture.

## docker image ls

Comme le docker ps, docker image ls permet de lister les images qui ont étaient téléchargée sur l'hote.

    root@master:/home/vagrant# docker image ls
    REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
    nginx               alpine              7ce0143dee37        3 days ago          22.8MB
    alpine              latest              021b3423115f        3 days ago          5.6MB
    nginx               stable              766b39f5021c        2 weeks ago         133MB

On notera la différence de taille entre une image nginx:alpine basée sur alpine:latest et une nginx:stable basée elle sur debian:buster  
Tout comme ps q, on a la possibilité de n'afficher que l'id avec _docker image ls -q_

un alias de docker image ls est docker images (avec un s a image) :

    root@master:/home/vagrant# docker images -q
    7ce0143dee37
    021b3423115f
    766b39f5021c

## docker image rm

On peut supprimer une image (qui n'est pas actuellement utilisée par un conteneur) en faisant un rm sur son repository:tag ou son ID.

    root@master:/home/vagrant# docker image rm hello-world:latest
    Untagged: hello-world:latest
    Untagged: hello-world@sha256:df5f5184104426b65967e016ff2ac0bfcd44ad7899ca3bbcf8e44e4461491a9e
    Deleted: sha256:d1165f2212346b2bab48cb01c1e39ee8ad1be46b87873d9ca7a4e434980a7726
    Deleted: sha256:f22b99068db93900abe17f7f5e09ec775c2826ecfe9db961fea68293744144bd

on peut supprimer toutes les images non utilisées d'un coup avec prune -a

    root@master:/home/vagrant# docker image prune -a

## docker commit

On peut creer une nouvelle couche avec les dernières modifications, mais il est fortement conseillé d'utilise un Dockerfile.

Dans cet exemple, on va installer vim sur une debian:buster-slim et ne pas oublier de nettoyer un peut le file system avant de creer l'image, elle sera moins grosse ne taille.

    root@master:/home/vagrant# docker run -ti --name mondebian debian:buster-slim
    root@7c6b322d059b:/# apt -qq update ; apt -qq --no-install-recommends install -y vim
    root@7c6b322d059b:/# apt-get clean ; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

Ensuite on sort du conteneur, on cherche l'id du conteneur de notre debian "mondebian"

    root@master:/home/vagrant# docker ps -a
    CONTAINER ID        IMAGE                COMMAND             CREATED             STATUS                     PORTS               NAMES
    7c6b322d059b        debian:buster-slim   "bash"              8 minutes ago       Exited (0) 3 minutes ago                       mondebian

Et on commit une nouvelle image basée sur nos modifications.

    root@master:/home/vagrant# docker commit -m "mon debian qui contient vim" 7c6b322d059b debianvim:v1.0
    sha256:ba63399f05d4cdf1f02eb3e7f87576aba8126e57973d4868c07baece20670ddc

l'option -m permet d'ajouter une descrition a l'iamge, suivi de l'id du conteneur modifié et on termine par un nom d'image:tag

et on peut lister les images pour voir notre nouvelle image apparaitre.

    root@master:/home/vagrant# docker images
    REPOSITORY          TAG                 IMAGE ID            CREATED              SIZE
    debianvim           v1.0                ba63399f05d4        About a minute ago   102MB
    debian              buster-slim         df0140a4030c        2 weeks ago          69.3MB

et maintenant on peut directement creer un nouveau conteneur qui tire sur cette image qui inclura notre modification, vim en l'occurence.

## docker history

La commande docker history permet d'afficher l'historiques des commandes d'une image ainsi que ces layer.

    root@master:/home/vagrant# docker history debianvim:v1.0
    IMAGE               CREATED             CREATED BY                                      SIZE                COMMENT
    442d2adeafc2        18 seconds ago      /bin/sh -c apt -qq update ; apt -qq --no-ins…   33.2MB
    df0140a4030c        2 weeks ago         /bin/sh -c #(nop)  CMD ["bash"]                 0B
    <missing>           2 weeks ago         /bin/sh -c #(nop) ADD file:45f5dfa135c848a34…   69.3MB

## docker diff

La commande docker diff est un équivalent, mais a comme cible un conteneur et non une image.

    root@master:/home/vagrant# docker diff mondebian

---
## **Le fichier Dockerfile**
---

## Les instructions de Dockerfile

| Instruction  |                                 action |
|--------------|---------------------------------------:|
|FROM          |                          Image de base |
|ENV           | Définition de varibles d'environnement |
|RUN*          | Exécution d'une commande
|COPY* / ADD*  | Copie une ressource de l'hote vers le conteneur|
|EXPOSE        | Expose un port de l'application|
|HEALTHCHECK   | Vérifie l'étant de santé de l'application|
|VOLUME        | Définition d'un volume pour la gestion des données|
|WORKDIR       | Définition du répertoire de travail|
|USE           | Utilisateur auquel appartient le processus du conteneur|
|ENTRYPOINT/CMD| La commande executée au démarrage du conteneur|

Les instructions avec asterix craient un nouveau layer.

## l'instruction FROM et RUN

Ce fichier Dockerfile permet de creer une image automatiquement modifiée, et c'est la méthode a suivre plutot que la version commit précédente.

Ce fichier aillant un nom fixe, on créra un repertoire avec le fichier a l'intérieur, commençont par un exemple très simple.

    root@master:/home/vagrant# cat Dockerfile
    FROM debian:buster-slim
    RUN apt -qq update ; apt -qq --no-install-recommends install -y vim ; apt-get clean ; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* 

Le FROM indique sur qu'elle image on démarre et RUN ajoute un nouveau layer a notre image, il faut donc éviter dans creer le plus possible, d'ou l'utilisation d'artifice comme ; ou && pour réduire le nombre.

Bien sur une commande doit impérativement rendre la main, d'ou l'option -y passé a apt install.

On va maintentant créer cette image avec build.

    root@master:/home/vagrant# docker build -t debianvim:v1.1 .

le . indique le context build qui est envoyé au docker daemon, tout le repertoire . est tar puis envoyé.

Si on veut qu'un fichier ou répertoire ne soit pas inclus dans le tar, il faut l'ajouter dans un fichier .dockerignore (exemple .git)

par defaut le fichier de construction est Dockerfile, mais on peut le changer avec -f /path/lefichierDocker

Et lors du listing des images, notre nouvelle image debianvim:v1.1 est bien apparue.

    root@master:/home/vagrant# docker images
    REPOSITORY          TAG                 IMAGE ID            CREATED              SIZE
    debianvim           v1.1                3fb24f314a5d        About a minute ago   102MB
    debianvim           v1.0                ba63399f05d4        12 minutes ago       102MB
    debian              buster-slim         df0140a4030c        2 weeks ago          69.3MB

A noter qu'il y a 2 façons d'écrire le parametre

    RUN echo "hello"

Celui que le connait déja, il est fait préfixé par "sh -c", c'est donc un shell qui execute la commande.

Entre crochet précise que ce n'est pas exécuté par un shell mais le binaire est directement appelé, l'équivalent de la commande précédente est donc

RUN ["/bin/sh", "-c", "echo", "Hello"]

## L'instruction CMD

On a le fichier Dockerfile suivant :

    FROM alpine:latest
    CMD ["ping", "1.1.1.1"]

La commande par defaut du conteneur sera donc ping 1.1.1.1 et c'est la commande par defaut que l'on passe après le nom de l'image.

    root@master:/home/vagrant# docker build -t testping:v1.0 .
    root@master:/home/vagrant# docker run -ti --name monping --rm testping:v1.0

il lance automatiquement la commande ping 1.1.1.1

    docker run -ti --name monping --rm testping:v1.0 sh

mais si on overight la commande avec sh, il lance bien le terminal, mais avec ps on se rend compte que ping n'est pas executé.

Il ne peut y avoir qu'une seule commande CMD par Dockerfile et CMD et vide si non défini.


## L'instruction ENTRYPOINT

entrypoint est un peut comme CMD, sauf que l'instruction n'interfaire pas sur le passage en parametre dans run comme le fait CMD.

Attention si on utilise entrypoint, le cmd sera concaténé au parametre, prenons le cas suivant :

    FROM alpine:latest
    ENTRYPOINT ["ping"]
    CMD ["1.1.1.1"]

le conteneur va donc executer ping 1.1.1.1 si on n'a pas passer un argurmant a run.

    root@master:/home/vagrant# docker build -t testping:v1.1 .
    root@master:/home/vagrant# docker run -ti --name monping --rm testping:v1.1
    PING 1.1.1.1 (1.1.1.1): 56 data bytes
    64 bytes from 1.1.1.1: seq=0 ttl=62 time=1.533 ms

Et que ce passe t'il si on surcharge cmd ?

    root@master:/home/vagrant# docker run -ti --name monping --rm testping:v1.1 8.8.8.8
    PING 8.8.8.8 (8.8.8.8): 56 data bytes
    64 bytes from 8.8.8.8: seq=0 ttl=113 time=15.510 ms

Ce coup ci, il execute a nouveau le ping de l'entry point, mais comme on a surchargé cmd, il passe donc le 8.8.8.8

A noter que l'entrypoint peut etre surcharger avec l'instruction --entrypoint lors du run.

## L'instruction ENV

Elle permet de définir une variable d'environnement a utiliser.

    FROM nginx:alpine
    ENV www /usr/share/nginx/html/
    WORKDIR ${www}

dans cette exemple, la variable d'environnement contient le path vers la racine du serveur web, on peut bien sur surcharger cette variable avec run.

## L'instruction COPY et ADD

Ces instructions font la meme chose, copier un fichier du hote vers la destination, attention ADD décompresse automatiquement les tar.gz donc pour éviter les erreurs préviligier COPY.

Un nouveau layer est créé lors de l'utilisation de COPY ou ADD.

En gros on peut ajouter par rapport a l'exemple sur le ENV la copy d'un fichier.

    COPY ./index.html ${www}

## L'instruction EXPOSE

Elle permet d'indiquer quel port sera exposé au host (pour etre surchargé avec -p)

Avec nginx par exemple

    EXPOSE 80

que l'on surchage avec un docker run -p PORT_HOST,PORT_CONTENEUR

## L'instruction VOLUME

le volume n'est pas stocker dans le Layer mais sur la machine hote, donc si on supprime le conteneur, les données ne seront pas touchées. (option -v de docker run)

## L'instruction HEALTHCHECK

Elle permet de tester la santé de notre conteneur et effectuer une action en cas de problème (exemple quitter)

    FROM alpine:latest
    RUN apk update; apk add curl
    HEALTHCHECK --interval=5s --timeout=3s --retries=3 CMD curl -f http://127.0.0.1:80 || exit 1

L'instruction prend plusieurs parametre et il executé ce qui suit CMD, dans notre cas si curl fail, le conteneur s'éteint.


## EXEMPLE : Un petit serveur web python

On va creer un serveur web en python3

    root@master:/home/vagrant# cat serveur.py
    import http.server
    import socketserver

    PORT = 80
    Handler = http.server.SimpleHTTPRequestHandler

    with socketserver.TCPServer(("", PORT), Handler) as httpd:
        print("Serving At Localhost PORT", PORT)
        httpd.serve_forever()

et un index.html qui contient ce que l'on veut.

Notre Dockerfile contient :

    FROM python:3.9-alpine
    RUN mkdir /var/www
    COPY serveur.py /var/www/
    COPY index.html /var/www/
    WORKDIR /var/www/
    EXPOSE 80
    CMD ["/usr/local/bin/python", "serveur.py"]

Je ne passe pas le path de serveur.py car WORKDIR se place dans le repertoire www ou est le serveur.py et index.html

On construit notre image

    docker build -t serveur:v1.0 -f Dockerfile .

Et on execute en n'oubliant pas de rediriger le 80 vers un port de l'hote.

    docker run -d --name test --rm -p 8080:80 serveur:v1.0

Un petit tour par le navigateur avec http://IP_HOTE:8080 pour vérifier le fonctionnement.

---
## **Multi-stage builds**
---

Il permet de construire notre image en plusieurs étapes enfin d'aléger l'image finale.

Exemple avec ce Main.java

    class Main {
        public static void main(String[] args) {
            System.out.println("Hello, World!"); 
        }
    }

En docker file classique, on compile le fichier avec la jdk puis on le lance avec la jre.

    FROM openjdk:11
    RUN mkdir -p /usr/src/myapp
    COPY *.java /usr/src/myapp/
    WORKDIR /usr/src/myapp/
    RUN javac Main.java
    CMD ["java", "Main"]

Problème, cette image est grosse vu qu'elle sert de build et de production, voici un équivalent en multi stages.

    FROM openjdk:11 as construction
    RUN mkdir -p /usr/src/myapp
    COPY *.java /usr/src/myapp/
    WORKDIR /usr/src/myapp/
    RUN javac Main.java

    FROM openjdk:11-jre-slim
    RUN mkdir -p /usr/src/myapp
    COPY --from=construction /usr/src/myapp/Main.class /usr/src/myapp/
    WORKDIR /usr/src/myapp
    CMD ["java", "Main"]

La 1ere étape build le fichier java avec une image completer et on utilise ce fichier buildé dans une image plus petite contenant que la JRE en prenant comme fichier source la 1er étape nommée construction dans l'exemple.

Une fois buildé, on voit une grosse différence de taille, la 1ere image fait 648Mo contre 221Mo pour celle qui contient que la JRE slim.

## docker image save

Cette commande permet de sauver une image dans un fichier .tar

    root@master:/home/vagrant# docker image save -o hellojava.tar hellojava:v1.1
    root@master:/home/vagrant# ls -la *.tar
    -rw------- 1 root root 224557056 Aug 10 23:12 hellojava.tar

## docker image load

Et bien sur on peut restaurer cette image.

    root@master:/home/vagrant# docker image rm hellojava:v1.1
    root@master:/home/vagrant# docker image load < hellojava.tar
