# Vagrant file

Vagrant est un outil d'automatisation de machine virtuelle, personnelement, je m'en sers pour faciliter le deploiment sur ma virtualbox de dev.

Quand on a une machine initialisé pour test ça va très bien, mais des que l'on en a plusieurs, c'est plus compliqué (typiquement test docker swarm)

## Le fichier de configuration Vagrantfile

Le fichier de configuration **Vagrantfile** est écrit en ruby.

Le truc qu'il faut savoir en ruby, les variables sont inversées, on a :

    Vagrant.configure("2") do |config|

En python on aurait fait

    config = Vagrant.configure("2")

Dans ce cas on indique que l'on utlise la version 2 du fichier de configuration.

    config.vm.define "node0" do |master|

Notre premiere machine se nome node0 et on met cet objet dans la variable master (bon vous avez compris)

    master.vm.box = "debian/bullseye64"

ici on indique qu'elle image on va utiliser, on peut trouver les images sur le site de [vagrantup](https://app.vagrantup.com/boxes/search)

Dans notre cas on part sur une [debian 10 "bullseye64"](https://app.vagrantup.com/debian)

    master.vm.hostname = "Node0"

On définie le hostname qu'aura cette machine

    master.vm.network "private_network", ip: "192.168.56.50"

Et l'ip privé sur le network de virtualbox qu'elle utilise.

    master.vm.provider :virtualbox do |v|

v reçoit l'objet driver virtual box necessaire a la modification des paramètres de la VM.

    config.vm.provision "shell", inline: <<-SHELL

Execute un shell une fois la box démarrée, et fait des opérations avec sudo (depuis l'utilisateur vagrant)

## vagrant up

Pour lancer nos machine virtuelle, il suffit de lancer

    PS D:\git\share\vagrant> vagrant up

## vagrant ssh

Et pour s'y connecter en ssh, il suffit de faire

    PS D:\git\share\vagrant> vagrant ssh node0

Ou node0 et ne nom de la machine déterminé avec config.vm.define

Ps : l'utilisateur par défaut est vagrant et le mot de passe vagrant

## vagrant destroy

Pour détruire les machines démarrées avec vagrant up il suffit de faire

    PS D:\git\share\vagrant> vagrant destroy -f
