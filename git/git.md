# **GIT AVANCE**

Ajoute des data au précédent commit

    $ git commit --amend

log des commit de la mdif locale (non visible sur git log)

    $ git reflog

revenir d'un commit

    $ git reset --hard NumeroDuCommit

Stash

mettre les fichier en backup

    $ git stash -m "Message du backup"

Lister les stash

    $ git stash list

Montrer un stash

    $ git stash show "La ref de list"

Restaurer le stash

    $ git stash pop